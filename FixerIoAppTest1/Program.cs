﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixerIoAppTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string jsondata = File.ReadAllText("json1.json");
            //Currency m = JsonConvert.DeserializeObject<Currency>(jsondata);

            var client = new RestClient("http://data.fixer.io/api");

            var request = new RestRequest("latest", Method.GET);
            request.AddParameter("access_key", "433fbcb5a0dcb6c508cdf1d0f39d5cf2");

            IRestResponse<Currency> response = client.Execute<Currency>(request);
            Currency c = response.Data;
        }
    }

    public class Currency
    {
        public bool Success { get; set; }
        public int Timestamp { get; set; }
        public string Base { get; set; }
        public string Date { get; set; }
        public Dictionary<string, double> Rates { get; set; }
    }
}
