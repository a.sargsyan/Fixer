﻿using System;
using System.Collections.Generic;

namespace FixerIoFramework
{
    public class Currency
    {
        public bool Success { get; set; }
        public int Timestamp { get; set; }
        public string Base { get; set; }
        public DateTime Date { get; set; }
        public Dictionary<string, double> Rates { get; set; }
    }
}
