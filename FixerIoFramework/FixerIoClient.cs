﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixerIoFramework
{
    public class FixerIoClient
    {
        private FixerIoClient()
        {
            _client = new RestClient("http://data.fixer.io/api");
        }

        public FixerIoClient(string accessKey)
            : this()
        {
            AccessKey = accessKey;
        }

        private RestClient _client;

        public string AccessKey { get; set; }
        public DateTime date = DateTime.Now;
        public string latest = "latest";

        public Currency GetLatest()
        {
            var request = new RestRequest(latest, Method.GET);
            request.AddParameter("access_key", AccessKey);

            IRestResponse<Currency> response = _client.Execute<Currency>(request);
            return response.Data;
        }


        //symbols=USD,AUD,CAD,PLN
        public Currency GetLatest(params string[] symbols)
        {
            var request = new RestRequest("latest", Method.GET);
            request.AddParameter("access_key", AccessKey);
            request.AddParameter("symbols", string.Join(",", symbols));

            IRestResponse<Currency> response = _client.Execute<Currency>(request);
            return response.Data;
        }

        public Currency GetLatest(params Symbols[] symbols)
        {
            var request = new RestRequest("latest", Method.GET);
            request.AddParameter("access_key", AccessKey);
            request.AddParameter("symbols", string.Join(",", symbols));

            IRestResponse<Currency> response = _client.Execute<Currency>(request);
            return response.Data;
        }
        public Currency GetHistory(string date, params Symbols[] symbols)
        {
            var request = new RestRequest(date, Method.GET);
            //request.AddUrlSegment(latest, date);
            request.AddParameter("access_key", AccessKey);
            request.AddParameter("symbols", string.Join(",", symbols));
            Currency tD = GetLatest(symbols);
            IRestResponse<Currency> response = _client.Execute<Currency>(request);

            return response.Data;
        }

        //Vasya
        public List<Currency> GetTimeseries(int days, params Symbols[] symbols)
        {
            var today = DateTime.Now;

            var res = new List<Currency>(days);
            for (int i = 0; i < days; i++)
            {
                if (today.DayOfWeek == DayOfWeek.Saturday)
                    today = today.AddDays(-1);
                if (today.DayOfWeek == DayOfWeek.Sunday)
                    today = today.AddDays(-2);

                res.Add( GetHistory(today.ToString("yyyy-MM-dd"), symbols));

                today = today.AddDays(-1);
            }
            return res;
        }
        public List<Currency> GetTimeseries(string startDate, string endDate, params Symbols[] symbols)
        {
            DateTime start = Convert.ToDateTime(startDate);
            DateTime last = Convert.ToDateTime(endDate);

            var res = new List<Currency>();
            while (start != last)
            {
                res.Add(GetHistory(start.ToString("yyyy-MM-dd"), symbols));

                start = start.AddDays(1);
            }
            return res;
        }

        public Currency GetLatestConvert(Symbols @base, params Symbols[] symbols)
        {
            Currency res1 = GetLatest(@base);
            Currency res2 = GetLatest(symbols);
            try
            {
                ConvertRate(res1, res2, @base);
            }
            catch (Exception e)
            {
                Console.WriteLine("Sorry!!!Something is wrong, {0} Exception caught.",e);
            }
            return res2;
        }
        static void ConvertRate(Currency rate1, Currency rate2, Symbols @base)
        {
            rate2.Base = @base.ToString();
            var resRate = new Dictionary<string, double>();

            foreach (var item in rate2.Rates.Keys)
                resRate.Add(item, rate2.Rates[item] / rate1.Rates[@base.ToString()]);

            rate2.Rates = resRate;
        }

        public Currency GetHistoryConvert(string date, Symbols @base, params Symbols[] symbols)
        {
            Currency res1 = GetHistory(date, @base);
            Currency res2 = GetHistory(date, symbols);
            try
            {
                ConvertRate(res1, res2, @base);
            }
            catch (Exception e)
            {
                Console.WriteLine("Sorry!!!Something is wrong, {0} Exception caught.", e);
            }
            return res2;
        }
        public Dictionary<string, double> GetChenged(List<Currency> sours)
        {
            var res = new Dictionary<string, double>();

            foreach (var item in sours[0].Rates.Keys)
                res.Add(item, sours[sours.Count - 1].Rates[item] - sours[0].Rates[item]);

            return res;
        }
    }
}
